var appRoot = 'src/';
var assetRoot = 'assets/';
var outputRoot = 'dist/';
var exportSrvRoot = 'export/';

module.exports = {
  root: appRoot,
  source: appRoot + '**/*.js',
  html: assetRoot + '**/*.html',
  css: assetRoot + '**/*.css',
  output: outputRoot,
  exportSrv: exportSrvRoot,
  doc: './doc',
};
