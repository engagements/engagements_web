define(['exports', 'aurelia-api'],
    function (exports, _aureliaAPI) {
        'use strict';

        Object.defineProperty(exports, "__esModule", {
            value: true
        });

        exports.Endpoint = exports.Config = exports.Rest = undefined;
        exports.configure = configure;

        var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) {
            return typeof obj;
        } : function (obj) {
            return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj;
        };
        
        var Rest = exports.Rest = _aureliaAPI.Rest

        var Config = exports.Config = _aureliaAPI.Config;

        var Endpoint = exports.Endpoint = _aureliaAPI.Endpoint;

        function getMongoFilters(criteria) {
            return "where=" + JSON.stringify(criteria);
        }

        function getRequestPath(resource, criteria) {
            if ((typeof criteria === 'undefined' ? 'undefined' : _typeof(criteria)) === 'object' && criteria !== null) {
                resource += '?' + getMongoFilters(criteria);
            } else if (criteria) {
                var hasSlash = resource.slice(-1) === '/';
                resource += '' + (hasSlash ? '' : '/') + criteria + (hasSlash ? '/' : '');
            }

            return resource;
        }

        var rest_request = Rest.prototype.request;
        Rest.prototype.request = function request(method, path, body) {
            return rest_request.apply(this, [method, path, body]).then(function (response) {
                // Remove HATEOAS
                if (response._items)
                {
                    return response._items;
                }
                return response;
            });
        };

        Rest.prototype.find = function find(resource, criteria, options) {
            return this.request('GET', getRequestPath(resource, criteria), undefined, options);
        };

        // POST does not use filtering

        Rest.prototype.update = function update(resource, criteria, body, options) {
            return this.request('PUT', getRequestPath(resource, criteria), body, options);
        };

        Rest.prototype.patch = function patch(resource, criteria, body, options) {
            return this.request('PATCH', getRequestPath(resource, criteria), body, options);
        };

        Rest.prototype.destroy = function destroy(resource, criteria, options) {
            return this.request('DELETE', getRequestPath(resource, criteria), undefined, options);
        };

        function configure(aurelia, configCallback) {
            var config = aurelia.container.get(Config);

            configCallback(config);
        }
    }
);