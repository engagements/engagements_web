import jQuery from 'jquery';
import 'bootstrap';

import * as entities from 'config/entities';
import authConfig from 'config/authentication';

export function configure(aurelia) {
    aurelia.use
        .standardConfiguration()
        .developmentLogging()

    .plugin('aurelia-api-libs', config => {
        config
            .registerEndpoint('api', 'http://127.0.0.1:5000/api/')
            .registerEndpoint('auth', 'http://127.0.0.1:5000/auth/')
            .setDefaultEndpoint('api');
    })

    .plugin('aurelia-orm', builder => {
        builder.registerEntities(entities);
    })

    .plugin('aurelia-authentication', baseConfig => {
        baseConfig.configure(authConfig);
    });

    aurelia.start().then(() => aurelia.setRoot());
}
