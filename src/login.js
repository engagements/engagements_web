﻿import {inject, computedFrom, useView} from 'aurelia-framework';
import {AuthService} from 'aurelia-authentication';

@inject(AuthService)
@useView('/templates/login.html')
export class Login {
    constructor(authService) {
        this.authService = authService;
    };

    heading = 'Login';

    email = '';
    password = '';

    @computedFrom('authService.authenticated')
    get authenticated() {
        return this.authService.authenticated;
    }

    login() {
        return this.authService.login(this.email, this.password)
            .then(response => {
                console.log("success logged: ");
                console.log(response);
            })
            .catch(err => {
                console.log("login failure: ");
                console.log(err);
            });
    };

    authenticate(name) {
        return this.authService.authenticate(name)
            .then(response => {
                console.log("auth response: ");
                console.log(response);
            });
    }
}
