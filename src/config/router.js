﻿import {inject} from 'aurelia-framework';
import {Router} from 'aurelia-router';
import {AuthenticateStep} from 'aurelia-authentication';

@inject(Router)
export default class {

    constructor(router) {
        this.router = router;
    }

    configure() {
        var appRouterConfig = function (config) {
            config.title = 'Engagements';

            config.addPipelineStep('authorize', AuthenticateStep);

            config.map([
                { route: '', moduleId: 'dashboard', nav: false, title: 'Dashboard' },

                { route: 'users', name: 'users', moduleId: 'users', nav: true, auth: true, title: 'Users' },

                { route: 'register', moduleId: 'register', nav: false, title: 'Register' },
                { route: 'login', moduleId: 'login', nav: false, title: 'Login' },
                { route: 'logout', moduleId: 'logout', nav: false, title: 'Logout' },
                { route: 'profile', moduleId: 'profile', nav: false, title: 'Profile' },

            ]);
        };

        this.router.configure(appRouterConfig);
    }

}
