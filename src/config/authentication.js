﻿export default {
    endpoint: 'auth',
    configureEndpoints: ['api'],

    loginUrl: 'login',
    signupUrl: 'register',
    profileUrl: 'profile',
    unlinkUrl: 'profile/unlink',

    loginOnSignup: false,

    authTokenType: 'Basic',

    expiredRedirect: 1
};
