import {inject, useView} from 'aurelia-framework';
import {EntityManager} from 'aurelia-orm';

@inject(EntityManager)
@useView('/templates/users.html')
export class Users {
    heading = 'Users';
    users = [];

    constructor(entityManager) {
        this.usersRepository = entityManager.getRepository('user');
        this.usersEntity = entityManager.getEntity('user');
    }

    activate() {
        return this.usersRepository.find()
            .then(users => {
                this.users = users;
            });
    }
}
