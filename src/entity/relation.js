﻿import {Entity, validatedResource, association} from 'aurelia-orm';
import {ensure} from 'aurelia-validation';

@validatedResource()
export class Relation extends Entity {
    data = null;
    
    @association()
    user = null;

    @association()
    organization = null;
}
