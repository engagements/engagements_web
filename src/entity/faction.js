﻿import {Entity, validatedResource, association} from 'aurelia-orm';
import {ensure} from 'aurelia-validation';

@validatedResource()
export class Faction extends Entity {
    @ensure(it => it.isNotEmpty())
    name = null;
}
