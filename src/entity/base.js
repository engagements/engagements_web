﻿import {Entity, validatedResource, association} from 'aurelia-orm';
import {ensure} from 'aurelia-validation';

@validatedResource()
export class Base extends Entity {
    @ensure(it => it.isNotEmpty())
    name = null;

    data = null;
    
    @association('user')
    owner = null;

    @association()
    region = null;
}
