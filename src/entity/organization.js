﻿import {Entity, validatedResource, association} from 'aurelia-orm';
import {ensure} from 'aurelia-validation';

@validatedResource()
export class Organization extends Entity {
    @ensure(it => it.isNotEmpty())
    name = null;
    
    @association()
    faction = null;

    @association()
    region = null;
}
