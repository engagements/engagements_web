﻿import {Entity, validatedResource, association} from 'aurelia-orm';
import {ensure} from 'aurelia-validation';

@validatedResource()
export class Operator extends Entity {
    @ensure(it => it.isNotEmpty())
    name = null;
    
    loyalty = null;
    
    health = null;
    
    points = null;
    
    data = null;
    
    locale = null;
    
    @association('user')
    employer = null;
}
