﻿import {Entity, validatedResource, association} from 'aurelia-orm';
import {ensure} from 'aurelia-validation';

@validatedResource()
export class User extends Entity {
    @ensure(it => it.isNotEmpty().hasLengthBetween(5, 254))
    email = null;
    
    password = null;
    
    active = null;
    
    confirmed_at = null;
    
    timezone = null;
    
    locale = null;
    
    @association('role')
    roles = [];
}
