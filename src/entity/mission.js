﻿import {Entity, validatedResource, association} from 'aurelia-orm';
import {ensure} from 'aurelia-validation';

@validatedResource()
export class Mission extends Entity {
    @ensure(it => it.isNotEmpty())
    name = null;

    data = null;
    
    @association()
    user = null;
}
