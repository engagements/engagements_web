import 'bootstrap';

import {inject, useView} from 'aurelia-framework';
import {Router} from 'aurelia-router';

import AppRouterConfig from 'config/router';

@inject(Router, AppRouterConfig)
@useView('/templates/app.html')
export class App {

    constructor(router, appRouterConfig) {
        this.router = router;
        this.appRouterConfig = appRouterConfig;
    }

    activate() {
        this.appRouterConfig.configure();
    }
}
