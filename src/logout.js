﻿import {inject, useView} from 'aurelia-framework';
import {AuthService} from 'aurelia-authentication';

@inject(AuthService)
@useView('/templates/logout.html')
export class Logout {
    constructor(authService) {
        this.authService = authService;
    };

    activate() {
        this.authService.logout("#/login")
            .then(response => {
                console.log("ok logged out on logout.js");
            })
            .catch(err => {
                console.log("error logged out logout.js");

            });
    }
}
